# CSS Selected Topics o How Css can blow your mind 🤨 Ch.1

En éste artículo reuno varios estilos css, que me han ayudado, se que me ayudarán o que tal vez no conocíamos y siempre los hemos visto. Espero les guste los ami 😘 

Nota: Éste artículo puede contener hartos hartos Emoji!, y tal vez unos cuantos memes
Nota2: Éste es el capítulo1

## Box-sizing: border-box; El Dwey ésta si es tu familia

Normalmente cuando agregamos un border a un elemento HTML, éste borde suma (lo del ancho y alto del borde) al ancho y alto de ese elemento caja html, cuando declaramos el border box. Estamos diciendo que este border pertenezca al ancho de el elemento

![Border-Box](box-sizing.png)

[boder-box-example](https://codepen.io/IvanCastelan/pen/MWgmWJJ)

## Pseudos para un Link

![pseudos](links.png)

[pseudos para link](https://codepen.io/IvanCastelan/pen/ZEzKYwP)

## BEM,  you're always running here and there

![ BEM ](https://media.giphy.com/media/Q6uvhhWdipgBi/giphy.gif)

Es un standard de buena práctica para hacer css 🤓 (Css tmbn rifa eh Omar), sus siglas significan 

### Block__Element--Modifier

![BEM](bem.png)

###### Por qué deberíamos usar BEM??

- [x] Es Fácil de Asimilar
- [x] Nos da uns estructura mantenible y escalable de css, CSS!!!

Ahora vamos a ver que es cada uno de los segmentos que lo componen

**Bloque**

La parte más externa de un elemento e sun bloque, contiene todas o la mayoría de características reutilizables

![bloque](bloque.png)

**Elemento**

Dentro del bloque puede haber uno o más elementos (divs, imps, o lo que sea) estos añaden nuevos estilos al bloque sin reescribir la característica.

![element](element.png)

**Modificador**

Son las variaciones de un bloque o elemento, pero que añaden nuevas características o estilos

![modifier](modifier.png)

### Arre! con los ejemplos 🤠

Un bloque simple es un elemento o una clase que adentro tenga muchas cosas

![bem-block](bem-block.png)

##### Bloques con Modificadores

A ver ahora con un modificador, los modificadores son los que agregan las nuevas  características a los elementos, entonces debemos dejar las reglas de la clase superior y hacer otra para los nuevos estilos

✅

![modifier1-bien](modifier1-bien.png)

❌

![modifier1-mal](modifier1-mal.png)



##### Bloques con Elementos

Donde leí dice que las estructuras complejas de html tienen elementos hijos. Cada elemento hijo necesita estilos en una clase css, uno de los propositos de BEM, es mantener la especificidad baja y consistente, no hay que omitir las clases de los hijos en el html, eso nos llevará a usar el selector con gran especificidad para elementos dentro del bloque



✅

![element1-bien](element1-bien.png)

❌

![element1-mal](element1-mal.png)

Si la estructura tiene muchos hijos, en muchos niveles de profundidad, no hay que representar cada nivel en el nombre de la clase 

Bem no intenta comunicar la profuncidad de la estructura, una clase css con BEM representa un elemento hijo en el componente y ya

✅

![profundidad1-bien](profundidad1-bien.png)

❌

![profundidad1-bien](profundidad1-mal.png)



##### Elementos con Modificador, el BEM Completo 

In some cases, you may want to change a single element in a component. In those cases, a modifier must be added to the element instead of the component.

En algunos casos, se quiere agregar un cambio solo a un elemento ahi chiquito en nuestro pan, y para eso usaríamos un ...  Modificador Duh!, pero se pone al Elemento en lugar de al component, siempre y cuando esté en la estructura html ;)

![bem-completo](bem-completo.png)

##### Estilos de elementos basados en el modificador

Si se están modificando elementos del mismo componente y con base en lo mismo, como buena practica se mueve el modificador al bloque padre de los elementos, y ya se llama con base en el modificador las nuevas clases de los elementos. Así se tiene un mejor control de la especificidad, y hace más fácil la modificación

✅

![especi-bien](especi-bien.png)

❌

![especi-mal](especi-mal.png)



## ::Before && ::After

### Before

Es un selector css que permite elegir un elemento html e insertar contenido antes de éste

### After

Es lo mismo pero después del elemento seleccionado

[Before y after](https://codepen.io/IvanCastelan/pen/wvwdBZy)

